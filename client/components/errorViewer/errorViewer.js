import {
    Meteor
    } from 'meteor/meteor';
import {
    Template
    } from 'meteor/templating';

import './errorViewer.html';

var template = Template.errorViewer;

template.onCreated(function() {
    template.error = this.data;
});

template.helpers({
    error: function() {
        return template.error.get();
    }
});

template.events({
    'click .close' () {
        template.error.set(null);
    }
});
import {
    Meteor
    } from 'meteor/meteor';
import {
    Template
    } from 'meteor/templating';
import {
    HTTP
    } from 'meteor/http'
import Collections from '/client/collections/collections.js'

import './applicationInfoModal.html';

var template = Template.applicationInfoModal;
var onClose;

template.onCreated(function() {
    Assert.notNull(this.data.publicKey, 'Property "publicKey" wasn\'t passed into template.', template.error);
    Assert.notNull(this.data.onClose, 'Function "onClose" wasn\'t passed into template.', template.error);

    onClose = this.data.onClose;
    template.publicKey = new ReactiveVar(this.data.publicKey);
    template.application = new ReactiveVar();
});

template.rendered = function() {
    $('#application-info-modal').modal('show');
    $('#application-info-modal').on('hidden.bs.modal', function () {
        onClose();
    });
}

template.destroyed=function(){
    $('#application-info-modal').modal('hide');
};

template.helpers({
    application: function() {
        return Applications.findOne({
            publicKey: template.publicKey.get()
        });
    },
    shareDevicesViewerData: function() {
        return {
            shareWith: template.publicKey.get()
        }
    }
});

template.events({
    'click .close' () {
        onClose();
    }
});


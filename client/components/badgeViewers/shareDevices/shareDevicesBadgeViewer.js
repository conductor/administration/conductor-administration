import {
    Meteor
    } from 'meteor/meteor';
import {
    Template
    } from 'meteor/templating';

import './shareDevicesBadgeViewer.html';

var template = Template.shareDevicesBadgeViewer;

template.onCreated(function() {
    Assert.notNull(this.data.shareWith, 'Property "shareWith" wasn\'t passed into template.', template.error);

    template.devices = Devices.find({});
    template.user = Session.get('user');
    template.shareWith = this.data.shareWith;
    template.error = new ReactiveVar(null);
});

template.helpers({
    isDeviceShared: function() {
        return isDeviceShared(this, template.shareWith) ? 'confirmed' : '';
    },
    application: function() {
        return Applications.findOne({
            'publicKey': template.shareWith
        });
    },
    error: function() {
        return template.error;
    }
});

function isDeviceShared(device, publicKey) {
    return _.contains(device.deviceAuthorizations, publicKey);
}

template.events({
    'click .toggle-share-device' () {
        Assert.notNull(this.id, 'Device id cannot be null.', template.error);

        if (isDeviceShared(this, template.shareWith)) {
            console.debug('Revoking device access to device ' + this.id + ' for user/application with id ' + template.shareWith + '.');

            Meteor.call('User.revokeDeviceAccess', Session.get('user').authenticationKey.privateKey, template.shareWith, this.id, function(error, result) {
                if (error) {
                    console.error('An error occured while revoking device access.', error);
                    template.error.set({ title: 'An error occured while revoking device access.', message: error.reason });
                } else {
                    console.debug('Successfully revoked device access.', result);
                }
            })
        } else {
            console.debug('Giving device access to device ' + this.id + ' for user/application with id ' + template.shareWith + '.');

            Meteor.call('User.giveDeviceAccess', Session.get('user').authenticationKey.privateKey, template.shareWith, this.id, function(error, result) {
                if (error) {
                    console.error('An error occured while giving device access.', error);
                    template.error.set({ title: 'An error occured while giving device access.', message: error.reason });
                } else {
                    console.debug('Device access was successfully given.', result);
                }
            })
        }
    }
});

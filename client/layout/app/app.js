import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';

import './app.html';

Template.mainLayout.helpers({
    user: function() {
        return Session.get('user');
    }
});

Template.mainLayout.events({
    'click .btn-logout' () {
        Session.setPersistent('user', null);
        FlowRouter.go('home');
    }
});
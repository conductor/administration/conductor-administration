ControlUnits = new Meteor.Collection('controlUnits');
Devices = new Meteor.Collection('devices');
Applications = new Meteor.Collection('applications');
InstallComponentGroupResponse = new Meteor.Collection('InstallComponentGroupResponses');

Meteor.autorun(() => {
    var getPrivateKey = () => Session.get('user') ? Session.get('user').authenticationKey.privateKey : undefined;

    Meteor.subscribe('control_units', {
        apiKey: getPrivateKey()
    });

    Meteor.subscribe('devices', {
        apiKey: getPrivateKey()
    });

    Meteor.subscribe('command.install_component_group_responses', {
        apiKey: getPrivateKey()
    });

    Meteor.subscribe('user.applications', {
        apiKey: getPrivateKey()
    });
});

Assert = {
    notNull: function(variable, message, reactiveErrorVariable) {
        if (variable === undefined) {
            if (reactiveErrorVariable) {
                reactiveErrorVariable.set({ title: 'Assertion failed', message: message });
            }
            throw new Error(message);
        }
    }
}
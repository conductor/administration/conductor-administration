FlowRouter.route('/', {
  name: 'home',
  action: function() {
    BlazeLayout.render("mainLayout", {content: "home"});
  }
});

var installationRoutes = FlowRouter.group({
  prefix: '/install',
  name: 'install',
});

installationRoutes.route('/', {
    name: 'installHome',
    action: function(params) {
      $.material.init()
      BlazeLayout.render( 'mainLayout', { content: 'install' } );
    }
});

installationRoutes.route('/control-unit', {
    name: 'installControlUnit',
    action: function(params) {
      $.material.init()
      BlazeLayout.render( 'mainLayout', { content: 'installControlUnit' } );
    }
});

installationRoutes.route('/device', {
    name: 'installDevice',
    action: function(params) {
      $.material.init()
      BlazeLayout.render( 'mainLayout', { content: 'installDevice' } );
    }
});

installationRoutes.route('/component', {
    name: 'installComponentGroup',
    action: function(params) {
      $.material.init()
      BlazeLayout.render( 'mainLayout', { content: 'installComponentGroup' } );
    }
});

FlowRouter.route('/invite/:inviteKey', {
    name: 'invite',
    action: function(params) {
      BlazeLayout.render( 'mainLayout', { content: 'invite' } );
    }
});

FlowRouter.route('/invite-completed/:inviteKey', {
    name: 'inviteCompleted',
    action: function(params) {
      BlazeLayout.render( 'mainLayout', { content: 'inviteCompleted' } );
    }
});

FlowRouter.route('/share-devices/:shareWith', {
    name: 'shareDevices',
    action: function(params) {
        BlazeLayout.render( 'mainLayout', { content: 'shareDevices' } );
    }
});

FlowRouter.route('/applications', {
    name: 'applications',
    action: function(params) {
        BlazeLayout.render( 'mainLayout', { content: 'applications' } );
    }
});

FlowRouter.route('/controlUnits', {
    name: 'controlUnits',
    action: function(params) {
        BlazeLayout.render( 'mainLayout', { content: 'controlUnits' } );
    }
});

FlowRouter.route('/devices', {
    name: 'devices',
    action: function(params) {
        BlazeLayout.render( 'mainLayout', { content: 'devices' } );
    }
});

FlowRouter.route('/components', {
    name: 'components',
    action: function(params) {
        BlazeLayout.render( 'mainLayout', { content: 'components' } );
    }
});

FlowRouter.route('/login', {
    name: 'login',
    action: function(params) {
      BlazeLayout.render( 'mainLayout', { content: 'login' } );
    }
});



import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';

import './installControlUnit.html';

var template = Template.installControlUnit;
var controlUnitParing = {
    url: 'localhost',
    port: 8001,
    serverUrl: window.location.hostname,
    serverPort: 3030
}

template.onCreated(function() {
    template.controlUnit = new ReactiveVar(null);
    template.currentStep = new ReactiveVar(1);
    template.error = new ReactiveVar(null);
});

template.helpers({
    controlUnit: function() {
        return template.controlUnit.get();
    },
    currentStep: function() {
        return template.currentStep.get();
    },
    isOnline: function() {
        var controlUnit = template.controlUnit.get();

        if (controlUnit) {
            var online = ControlUnits.findOne({ publicKey: controlUnit.publicKey, online: true });
            return online;
        }
    },
    error: function() {
        return template.error;
    }
});

template.events({
    'submit #enter-name' (event) {
        event.preventDefault();
        var name = event.target.name.value;

        Meteor.call('Install.installControlUnit', Session.get('user').authenticationKey.privateKey, {
            controlUnitName: name
        }, function(error, result) {
            if (error) {
                console.error('An error occured while installing control unit.', error);
                template.error.set({ title: 'An error occured while installing control unit.', message: error.reason });
            } else {
                console.debug('Successfully installed control unit.');
                template.controlUnit.set(result);
                nextStep();
            }
        });
    },
    'click .btn-download' (event) {
        console.debug('Downloading control unit.');
        Assert.notNull(Meteor.settings.public.controlUnitInstallationDownload, 'Download url is undefined.', template.error);
        window.open(Meteor.settings.public.controlUnitInstallationDownload);
    },
    'click .btn-continue' () {
        nextStep();
    },
    'click .btn-sync' () {
        var controlUnit = template.controlUnit.get();

        Assert.notNull(controlUnit, 'Cannot pair control unit. Control unit is undefined.', template.error);
        Assert.notNull(controlUnit.privateKey, 'Cannot pair control unit. Control unit private key is undefined.', template.error);

        Assert.notNull(Meteor.settings.public.controlUnitPairingUrl, 'Cannot pair control unit. Control unit pairing url is undefined.', template.error);
        Assert.notNull(Meteor.settings.public.serverHostname, 'Cannot pair control unit. Server url is undefined.', template.error);
        Assert.notNull(Meteor.settings.public.serverPort, 'Cannot pair control unit. Server port is undefined.', template.error);
        Assert.notNull(Meteor.settings.public.serverBasePath, 'Cannot pair control unit. Server base path is undefined.', template.error);
        Assert.notNull(Meteor.settings.public.serverUseSSL, 'Cannot pair control unit. Server use ssl property is undefined.', template.error);

        console.debug('Pairing control unit.', controlUnit);
        window.open('http://' + Meteor.settings.public.controlUnitPairingUrl + '/pairing/request/?ip=' + Meteor.settings.public.serverHostname + '&port=' + Meteor.settings.public.serverPort + '&basePath=' + Meteor.settings.public.serverBasePath + '&useSSL=' + Meteor.settings.public.serverUseSSL+ '&apiKey=' + controlUnit.privateKey, '_blank', 'width=' + popupWindow.width() + ',height=' + popupWindow.height() + ',left=' + popupWindow.left() + ',top=' + popupWindow.top());
    },
    'click .btn-cancel' () {
        console.debug('Canceling control unit installation.');
        FlowRouter.go('installHome');
    }
});

function nextStep() {
    var currentStep = template.currentStep.get();
    template.currentStep.set(currentStep + 1);
}

var popupWindow = {
    left: function() {
        return window.outerWidth / 2 - popupWindow.width() / 2;
    },
    top: function() {
        return (window.outerHeight / 2 - popupWindow.height() / 2) * 0.33;
    },
    width: function() {
        return 650;
    },
    height: function() {
        return 250;
    }
}
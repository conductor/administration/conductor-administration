import {
    Meteor
    } from 'meteor/meteor';
import {
    Template
    } from 'meteor/templating';
import {
    HTTP
    } from 'meteor/http'
import Collections from '/client/collections/collections.js'

import './devices.html';

var template = Template.devices;

template.onCreated(function() {
    template.error = new ReactiveVar(null);
});

template.helpers({
    devices: function() {
        return Devices.find();
    },
    error: function() {
        return template.error;
    }
});

template.events({
    'click .info' (event) {
        console.debug('Show device info: ', this);
    },
    'click .uninstall' (event) {
        Assert.notNull(this.id, 'Device id cannot be null.', template.error);

        console.debug('Uninstalling device.', this);

        Meteor.call('Install.uninstallDevice', Session.get('user').authenticationKey.privateKey, { deviceId: this.id }, function(error, result) {
            if (error) {
                console.error('An error occured while uninstalling device.', error);
                template.error.set({ title: 'An error occured while uninstalling device.', message: error.reason });
            } else {
                console.debug('Successfully uninstalled device. ', result);
            }
        })
    }
});


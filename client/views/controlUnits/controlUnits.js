import {
    Meteor
    } from 'meteor/meteor';
import {
    Template
    } from 'meteor/templating';
import {
    HTTP
    } from 'meteor/http'
import Collections from '/client/collections/collections.js'

import './controlUnits.html';

var template = Template.controlUnits;

template.onCreated(function() {
    template.error = new ReactiveVar(null);
});

template.helpers({
    controlUnits: function() {
        return ControlUnits.find();
    },
    error: function() {
        return template.error;
    }
});

template.events({
    'click .info' (event) {
        console.debug('Show control unit info: ', this);
    },
    'click .uninstall' (event) {
        Assert.notNull(this.id, 'Control unit id cannot be null.', template.error);

        console.debug('Uninstalling control unit.', this);

        Meteor.call('Install.uninstallControlUnit', Session.get('user').authenticationKey.privateKey, { controlUnitId: this.id }, function(error, result) {
            if (error) {
                console.error(error);
                template.error.set({ title: 'An error occured while uninstalling control unit.', message: error.reason });
            } else {
                console.debug('Successfully uninstalled control unit. ', result);
            }
        })
    }
});


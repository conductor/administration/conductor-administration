import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';
import {
    HTTP
} from 'meteor/http'
import Collections from '/client/collections/collections.js'

import './installComponentGroup.html';

var template = Template.installComponentGroup;

template.onCreated(function() {
    template.error = new ReactiveVar(null);
    template.searchResult = new ReactiveVar(null);
    template.currentStep = new ReactiveVar(1);
    template.device = new ReactiveVar();
    template.componentGroup = new ReactiveVar();
    template.installationRequestId = new ReactiveVar(false);
});

template.helpers({
    searchResult: function() {
        return template.searchResult.get();
    },
    name: function() {
        return capitalizeFirstLetter(this.name);
    },
    description: function() {
        return capitalizeFirstLetter(this.description);
    },
    currentStep: function() {
        return template.currentStep.get();
    },
    devices: function() {
        return Devices.find();
    },
    device: function() {
        return template.device.get();
    },
    componentGroup: function() {
        return template.componentGroup.get();
    },
    installed: function() {
        var requestId = template.installationRequestId.get();

        if (requestId) {
            return InstallComponentGroupResponse.find({
                requestId: requestId,
                status: "SUCCESS"
            }).fetch();
        }
    },
    wellSize: function() {
        return template.currentStep.get() == 3 ? 'large-well' : 'small-well';
    },
    error: function() {
        return template.error;
    }
});

template.events({
    'input .search-box' (event) {
        if (event.currentTarget.value) {
            search(event.currentTarget.value);
        } else {
            template.searchResult.set(null);
        }
    },
    'click .btn-continue' () {
        event.preventDefault();
        nextStep();
    },
    'click .select-component-group' () {
        console.debug('Selected component group: ', this);

        // If no component group options exists then jump to the next step in the installation process.
        if (this.options.length == 0) {
            nextStep();
        }

        setOptionValues(this);
        template.componentGroup.set(this);
    },
    'click .select-device' () {
        console.debug('Selected device: ', this);
        template.device.set(this);
    },
    'click .install' () {
        event.preventDefault();
        var device = template.device.get();
        var componentGroup = template.componentGroup.get();

        Assert.notNull(device, 'Cannot install component group on a null device.', template.error);
        Assert.notNull(device.id, 'Device id cannot be null.', template.error);
        Assert.notNull(componentGroup, 'Component group is null.', template.error);
        Assert.notNull(Meteor.settings.public.repositoryProtocol, 'Repository protocol is null.', template.error);
        Assert.notNull(Meteor.settings.public.repositoryUrl, 'Repository url is null.', template.error);

        componentGroup.repositoryUrl = Meteor.settings.public.repositoryProtocol + '://' + Meteor.settings.public.repositoryUrl + '/api';

        var install = {
            deviceId: device.id,
            componentGroup: componentGroup
        };

        console.debug('Installing component group.', install);

        Meteor.call('Install.installComponentGroup', Session.get('user').authenticationKey.privateKey, install, function(error, result) {
            if (error) {
                console.error('An error occured while installing component group', error);
                template.error.set({ title: 'An error occured while installing component group', message: error.reason });
            } else {
                console.debug('Installed component group successfully.', result);
                template.installationRequestId.set(result.requestId);
            }
        });
    },
    'keyup .transform-option-value' (event) {
        try {
            this.value = transformValue(event.target.value, this.dataType);
            $(event.target.parentElement).removeClass("has-warning");
        } catch (error) {
            $(event.target.parentElement).addClass("has-warning");
        }
    },
    'click .btn-cancel' () {
        console.debug('Canceling component group installation.');
        FlowRouter.go('installHome');
    }
});

function nextStep() {
    var currentStep = template.currentStep.get();
    template.currentStep.set(currentStep + 1);
}

function search(query) {
    Assert.notNull(Meteor.settings.public.repositoryProtocol, 'Can\'t search in repository. Repository protocol is null.', template.error);
    Assert.notNull(Meteor.settings.public.repositoryUrl, 'Can\'t search in repository. Repository url is null.', template.error);

    HTTP.post(Meteor.settings.public.repositoryProtocol + '://' + Meteor.settings.public.repositoryUrl + '/api/search', {
        data: {
            simpleQuery: query
        }
    }, function(error, result) {
        if (error) {
            console.error('An error occured while searching in repository.', error);
            template.error.set({ title: 'An error occured while searching in repository.', message: error.reason });
        } else {
            console.debug('Result from repository: ', result);
            template.searchResult.set(result.data);
        }
    });
}

function transformValue(string, dataType) {
    switch(dataType) {
        case 'double':
            var value = parseFloat(string);

            if (isNaN(value)) {
                throw 'Not a valid number.';
            }

            return value;
        case 'integer':
            var value = parseInt(string);

            if (isNaN(value)) {
                throw 'Not a valid number.';
            }

            return value;
        case 'string':
            return string;
        default:
            throw 'Unsupported data type, can\'t transform string to ' + dataType + '.';
    }
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function setOptionValues(componentGroup) {
    // Set option value for component group options
    componentGroup.options.forEach(function(option) {
        setOptionValue(option);
    });

    // Set option value for component options
    componentGroup.components.forEach(function(component) {
        component.options.forEach(function(option) {
            setOptionValue(option);
        });
    });
}

function setOptionValue(option) {
    if (option.defaultValue) {
        option.value = option.defaultValue;
    } else {
        switch(option.dataType) {
            case 'boolean':
                option.value = false;
                break;
            case 'double':
            case 'integer':
                option.value = 0;
                break;
            case 'string':
                option.value = "";
                break;
        }
    }
}

InstallComponentGroupResponse.find({}).observe({
    added: function(installationResponse) {
        console.debug('Installation response.', installationResponse);
    },
    changed: function(installation) {
        var requestId = template.installationRequestId.get();

        if (requestId && requestId == installation.requestId && installation.status == "ERROR") {
            template.error.set({ title: 'Component group installation failed.', message: installation.errorMessage });
        }

        console.debug('Installation changed.', installation);
    }
})

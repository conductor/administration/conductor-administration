import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';

import './shareDevices.html';

var template = Template.shareDevices;

template.onCreated(function() {
    template.shareWith = FlowRouter.getParam('shareWith');
});

template.helpers({
    application: function() {
        return Applications.findOne({
            'publicKey': template.shareWith
        });
    },
    badgeViewerData: function() {
        return {
            shareWith: template.shareWith
        }
    }
});

template.events({
    'click .finish' (event) {
        var onFinish = FlowRouter.current().queryParams['onFinish'];
        console.log(onFinish);

        if (onFinish) {
            FlowRouter.go(onFinish);
        } else {
            FlowRouter.go('home');
        }

        event.preventDefault();
    }
});

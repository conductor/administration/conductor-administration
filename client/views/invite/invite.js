import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';

import './invite.html';

var template = Template.invite;

template.onCreated(function() {
    if (!Session.get('user')) {
        log.error('Couldn\'t find logged in user.');
        template.error.set('Couldn\'t find logged in user.');
        return;
    }

    template.applicationInviteKey = FlowRouter.getParam('inviteKey');
    template.user = Session.get('user');
    template.error = new ReactiveVar(null);
    template.isInviteAccepted = new ReactiveVar(false);
    template.applicationInvite = new ReactiveVar(null);
    getApplicationInvite();
});

template.helpers({
    applicationInvite: function() {
        return template.applicationInvite.get();
    },
    error: function() {
        return template.error;
    }
});

template.events({
    'click .btn-accept' (event) {
        Assert.notNull(template.applicationInviteKey, 'Cannot accept application invite. Aplication invite key is undefined.', template.error);

        Meteor.call('User.acceptApplicationInvite', template.user.authenticationKey.privateKey, template.applicationInviteKey, function(error, result) {
            if (error) {
                console.error('Accept application invite request failed', error);
                template.error.set({ title: 'An error occured while accepting application invite.', message: error.reason });
            } else {
                console.debug('Application invite accepted successfully.', result);
                shareDevices();
            }
        });

        event.preventDefault();
    },
    'click .btn-cancel' () {
        FlowRouter.go('home');
    }
});

function getApplicationInvite() {
    Assert.notNull(template.applicationInviteKey, 'Cannot get application invite. Aplication invite key is undefined.', template.error);

    Meteor.call('User.getApplicationInvite', template.user.authenticationKey.privateKey, template.applicationInviteKey, function(error, result) {
        if (error) {
            template.error.set({ title: 'Failed to get application invite.', message: error.reason });
            console.error('Failed to get application invite.', error);
        } else {
            console.debug('Application invite: ', result);
            template.applicationInvite.set(result);
        }
    });
}

function shareDevices() {
    var queryParams = FlowRouter.current().queryParams;
    queryParams['onFinish'] = '/invite-completed/' + FlowRouter.getParam('inviteKey');
    var url = FlowRouter.path('/share-devices/' + template.applicationInvite.get().application.publicKey, FlowRouter.current().params, queryParams);
    console.debug('Redirecting to share device url: ', url);
    FlowRouter.redirect(url);
}

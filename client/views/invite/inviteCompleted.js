import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';

import './inviteCompleted.html';

var template = Template.inviteCompleted;

template.onCreated(function(){
    template.error = new ReactiveVar(null);
    var inviteKey = FlowRouter.getParam('inviteKey');
    var user = Session.get('user');

    Meteor.call('User.applicationInviteCompleted', user.authenticationKey.privateKey, inviteKey, function(error, result) {
        if (error) {
            console.error('Failed to complete application invite: ', error);
            template.error.set({ title: 'Failed to complete application invite: ', message: error.reason });
        } else {
            console.debug('Successfully completed application invite: ', result);
        }
    });
});

template.helpers({
    error: function() {
        return template.error;
    }
});
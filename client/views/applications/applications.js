import {
    Meteor
    } from 'meteor/meteor';
import {
    Template
    } from 'meteor/templating';
import {
    HTTP
    } from 'meteor/http'
import Collections from '/client/collections/collections.js'

import './applications.html';

var template = Template.applications;

template.onCreated(function() {
    template.applications = new ReactiveVar();
    template.showApplicationInfo = new ReactiveVar(null);
    template.applicationInfo = new ReactiveVar(null);
    template.error = new ReactiveVar(null);
});

template.helpers({
    applications: function() {
        return Applications.find();
    },
    showApplicationInfo: function() {
        return template.showApplicationInfo.get();
    },
    applicationInfo: function() {
        return {
            publicKey: template.applicationInfo.get(),
            onClose: function() {
                template.showApplicationInfo.set(false);
            }
        };
    },
    error: function() {
        return template.error;
    }
});

template.events({
    'click .info' (event) {
        console.debug('Show application info.', this);
        template.applicationInfo.set(this.publicKey);
        template.showApplicationInfo.set(true);
    }
});


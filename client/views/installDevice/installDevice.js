import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';
import {
    HTTP
} from 'meteor/http'
import Collections from '/client/collections/collections.js'

import './installDevice.html';

var template = Template.installDevice;

template.onCreated(function() {
    template.error = new ReactiveVar(null);
    template.searchResult = new ReactiveVar(null);
    template.currentStep = new ReactiveVar(1);
    template.device = new ReactiveVar();
    template.controlUnit = new ReactiveVar();
    template.installed = new ReactiveVar(false);
});

template.helpers({
    error: function() {
        return template.error;
    },
    searchResult: function() {
        return template.searchResult.get();
    },
    name: function() {
        return capitalizeFirstLetter(this.name);
    },
    description: function() {
        return capitalizeFirstLetter(this.description);
    },
    currentStep: function() {
        return template.currentStep.get();
    },
    controlUnits: function() {
        return ControlUnits.find();
    },
    controlUnit: function() {
        return template.controlUnit.get();
    },
    device: function() {
        return template.device.get();
    },
    installed: function() {
        return template.installed.get();
    },
    wellSize: function() {
        return template.currentStep.get() == 3 ? 'large-well' : 'small-well';
    }
});

template.events({
    'input .search-box' (event) {
        if (event.currentTarget.value) {
            search(event.currentTarget.value);
        } else {
            template.searchResult.set(null);
        }
    },
    'keyup .device-name' (event) {
        var device = template.device.get();

        Assert.notNull(device, 'Cannot set device name. Device is undefined.', template.error);

        device.name = event.target.value;
        template.device.set(device);
    },
    'click .btn-continue' () {
        nextStep();
    },
    'click .select-device' () {
        console.debug('Selected device.', this);
        template.device.set(this);
    },
    'click .select-control-unit' () {
        console.debug('Selected control unit.', this);
        template.controlUnit.set(this);
    },
    'click .install' () {
        event.preventDefault();
        var device = template.device.get();
        var controlUnit = template.controlUnit.get();

        Assert.notNull(device, 'Cannot install device, device is undefined.', template.error);
        Assert.notNull(device.name, 'Cannot install device, device name is undefined.', template.error);
        Assert.notNull(device.type, 'Cannot install device, device type is undefined.', template.error);
        Assert.notNull(controlUnit, 'Cannot install device, control unit is undefined.', template.error);
        Assert.notNull(controlUnit.id, 'Cannot install device, control unit id is undefined.', template.error);

        var install = {
            controlUnitId: controlUnit.id,
            deviceName: device.name,
            deviceType: device.type
        };

        console.debug('Installing device:', install);

        Meteor.call('Install.installDevice', Session.get('user').authenticationKey.privateKey, install, function(error, result) {
            if (error) {
                console.error(error);
                template.error.set({ title: 'An error occured while installing device.', message: error.reason });
            } else {
                console.debug('Successfully installed device: ', result);
                template.installed.set(true);
            }
        });
    },
    'click .btn-cancel' () {
        console.debug('Canceling device installation.');
        FlowRouter.go('installHome');
    }
});

function nextStep() {
    var currentStep = template.currentStep.get();
    template.currentStep.set(currentStep + 1);
}

function search(query) {
    Assert.notNull(Meteor.settings.public.repositoryProtocol, 'Can\'t search in repository. Repository protocol is null.', template.error);
    Assert.notNull(Meteor.settings.public.repositoryUrl, 'Can\'t search in repository. Repository url is null.', template.error);

    HTTP.post(Meteor.settings.public.repositoryProtocol + '://' +  Meteor.settings.public.repositoryUrl + '/api/search', {
        data: {
            simpleQuery: query
        }
    }, function(error, result) {
        if (error) {
            console.error('An error occured while searching in repository.', error);
            template.error.set({ title: 'An error occured while searching in repository.', message: error.reason });
        } else {
            console.debug('Result from repository: ', result);
            template.searchResult.set(result.data);
        }
    });
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
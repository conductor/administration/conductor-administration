import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';

import './login.html';

var template = Template.login;

template.onCreated(function(){
    template.error = new ReactiveVar(null);
});

template.helpers({
    error: function() {
        return template.error;
    }
});

template.events({
    'submit form' (event, template) {
        var form = event.target;

        Meteor.call('User.login', form.email.value, form.password.value, function(error, result) {
            if (error) {
                console.error('Invalid email and/or password.', error);
                template.error.set({ title: 'Invalid email and/or password.', message: error.reason });
            } else {
                console.debug('Successfully logged in.');
                Session.setPersistent('user', result);
                if (FlowRouter.current().queryParams.redirectUrl) {
                    FlowRouter.go(FlowRouter.current().queryParams.redirectUrl);
                } else {
                    FlowRouter.go('home');
                }
            }

            form.password.value = '';
        });

        event.preventDefault();
    }
});

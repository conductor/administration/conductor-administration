import {
    Meteor
    } from 'meteor/meteor';
import {
    Template
    } from 'meteor/templating';
import {
    HTTP
    } from 'meteor/http'
import Collections from '/client/collections/collections.js'

import './components.html';

var template = Template.components;

template.onCreated(function() {
    template.error = new ReactiveVar(null);
});

template.helpers({
    devices: function() {
        return Devices.find();
    },
    error: function() {
        return template.error;
    }
});

template.events({
    'click .info' (event) {
        console.debug('Show component info. ', this);
    },
    'click .uninstall' (event) {
        Assert.notNull(this.id, 'Component id cannot be null.', template.error);

        console.debug('Uninstalling component. ', this);

        Meteor.call('Install.uninstallComponent', Session.get('user').authenticationKey.privateKey, { componentId: this.id }, function(error, result) {
            if (error) {
                console.error('An error occured while uninstalling component.', error);
                template.error.set({ title: 'An error occured while uninstalling component.', message: error.reason });
            } else {
                console.debug('Successfully uninstalled component. ', result);
            }
        })
    }
});


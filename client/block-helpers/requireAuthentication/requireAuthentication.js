import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';

import './requireAuthentication.html';

Template.requireAuthentication.helpers({
    user: function() {
        if (!Session.get('user')) {
            redirectToLogin();
        }

        return Session.get('user');
    }
});

function redirectToLogin() {
    console.log('Redirecting to login screen.');

    // Redirect to login page, and pass on the redirect url, so we can go back to this page when the login is completed.
    var queryParams = FlowRouter.current().queryParams;
    queryParams['redirectUrl'] = FlowRouter.current().path;
    var url = FlowRouter.path('login', FlowRouter.current().params, queryParams);
    FlowRouter.go(url);
}

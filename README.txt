http://stackoverflow.com/a/30864393

Changed how we connect to the backend, so instead of connecting using the sockjs protocol (http://localhost:3000/sockjs/g2oi24t/websocket) we instead connect to (http://localhost:3030/websocket).
This is because the conductor backend doesn't work the same was as the sockjs protocol.

The file that has been modified is packages/ddp-client-custom/sockjs-0.3.4.js:

    var WebSocketTransport = SockJS.websocket = function(ri, trans_url) {
        var that = this;
        var url = trans_url + '/websocket';
        if (url.slice(0, 5) === 'https') {
            url = 'wss' + url.slice(5);
        } else {
            url = 'ws' + url.slice(4);
        }
        that.ri = ri;
        that.url = url;
        var Constructor = _window.WebSocket || _window.MozWebSocket;


        that.ws = new Constructor('ws://localhost:3030/websocket');

        that.ws.onopen = function() {
            that.ri._dispatchOpen();
        };

        that.ws.onmessage = function(e) {
            var payload = JSON.parse(e.data);

            if (Array.isArray(payload)) {
                for (var i = 0; i < payload.length; i++) {
                    that.ri._dispatchMessage(JSON.stringify(payload[i]));
                }
            } else {
                that.ri._dispatchMessage(JSON.stringify(payload));
            }
        };

        // Firefox has an interesting bug. If a websocket connection is
        // created after onunload, it stays alive even when user
        // navigates away from the page. In such situation let's lie -
        // let's not open the ws connection at all. See:
        // https://github.com/sockjs/sockjs-client/issues/28
        // https://bugzilla.mozilla.org/show_bug.cgi?id=696085
        that.unload_ref = utils.unload_add(function() {
            that.ws.close()
        });
        that.ws.onclose = function() {
            that.ri._didClose(1006, "WebSocket connection broken");
        };
    };

    WebSocketTransport.prototype.doSend = function(data) {
        this.ws.send(JSON.parse(data));
    };
# Conductor Administration
Administration app for Conductor.

## Setup Instructions
- Change configuration file "client/config/config.js"
- Double click on "start.bat".
- Open http://localhost:3000.

## Features
- Register new users
- Managing control units
- Managing devices
- Managing component groups
- Register new application
- Accepting application invites